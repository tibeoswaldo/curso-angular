import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReservasListadosComponent } from './reservas-listados/reservas-listados.component';
import { ReservasDetalleComponent } from './reservas-detalle/reservas-detalle.component';

const routes: Routes = [
  { path: 'reservas', component: ReservasListadosComponent },
  { path: 'reservas/:id', component: ReservasDetalleComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservasRoutingModule { }
