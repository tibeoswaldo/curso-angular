import { Component, OnInit } from '@angular/core';
import { ReservasApiClientService } from './../reservas-api-client.service';
@Component({
  selector: 'app-reservas-listados',
  templateUrl: './reservas-listados.component.html',
  styleUrls: ['./reservas-listados.component.css']
})
export class ReservasListadosComponent implements OnInit {
  api: any;
  constructor(private reservasApiClient: ReservasApiClientService ) { 
    this.api = this.reservasApiClient;
  }

  ngOnInit(): void {
  }

}
