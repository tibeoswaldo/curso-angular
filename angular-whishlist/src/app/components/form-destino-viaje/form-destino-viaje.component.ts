import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { fromEvent } from 'rxjs';
import { map, filter,debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  minLongitud = 3;
  fg: FormGroup;
  searchResults: string[];

  // forwardRef = evita la referencia circular que existe
  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) { 
    this.onItemAdded = new EventEmitter();    
    this.fg = fb.group({
      nombre: ['', 
                Validators.compose([
                  Validators.required,
                  this.nombreValidator,
                  this.nombreValidatorParametrizable(this.minLongitud)
                ])
              ],
      url: ['', 
              Validators.compose([
                Validators.required,
                this.nombreValidatorURL()
              ]) 
           ]
    });
    
    this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio el formulario: ', form);
      console.log(form);
    });
  }

  ngOnInit(): void {
    const elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
      .pipe(
        map((e:KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length > 3),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
      ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
  }

  guardar(nombre: string, url: string): boolean {
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl): {[s: string]: boolean} {
    const l = control.value.toString().length;
    if(l > 0 && l < 5){
      return { invalidNombre: true };
    } 
    return null;
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [s:string]: boolean } | null => {
      const l = control.value.toString().length;
      if(l > 0 && l < minLong){
        return { minLongNombre: true };
      } 
      return null;
    }
  }

  nombreValidatorURL(): ValidatorFn {
    const URL_REGEXP = /^(https?|http?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
    return (control: FormControl): { [s:string]: boolean } | null => {
      const dato = control.value;
      if(!URL_REGEXP.test(dato)){
        return { validURL: true };
      }
      return null;
    }
  }
}
