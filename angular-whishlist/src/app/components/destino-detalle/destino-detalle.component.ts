import { Component, OnInit, InjectionToken, Inject, Injectable } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
/*
class DestinosApiClientX {
  getById(id: string): DestinoViaje {
    console.log('llamando por la calse vieja!');
    return null;
  }
}

interface AppConfig {
  apiEndpoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'mi_api.com'
};
const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
*/
@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [DestinosApiClient],
  styles: [`
      mgl-map {
        height: 75vh;
        width: 75vw;
      }
  `]
  /*
  providers: [
        { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
        { provide: DestinosApiClientX, useExisting: DestinosApiClient }
      ]
      */
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;

  style = {
    sources: {
      world: {
        type: "geojson",
        data: "https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json"
      }
    },
    version: 8,
    layers: [{
      "id": "countries",
      "type": "fill",
      "source": "world",
      "layout": {},
      "paint": {
        'fill-color': '#6F788A'
      }
    }]
  };

  //constructor(private route: ActivatedRoute, private destinoApiClient: DestinosApiClient) { }
  //constructor(private route: ActivatedRoute, private destinoApiClient: DestinosApiClientX) { }
  constructor(private route: ActivatedRoute, private destinoApiClient: DestinosApiClient) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinoApiClient.getById(id);
  }
}
